use std::{io, process::Command};

fn main() -> io::Result<()> {
    let gresource_out = "com.gitlab.miridyan.Mt.gresource";
    let gresource_xml = "com.gitlab.miridyan.Mt.gresource.xml";
    let cargo_target_dir = option_env!("CARGO_TARGET_DIR").unwrap_or("../target");

    println!("cargo:rustc-env=GRESOURCE_OUT_DIR={}", cargo_target_dir);
    println!("cargo:rerun-if-changed=data/{}", gresource_xml);
    println!("cargo:rerun-if-changed={}/{}", cargo_target_dir, gresource_out);

    Command::new("glib-compile-resources")
        .current_dir("data")
        .arg(format!("--target={}/{}", cargo_target_dir, gresource_out))
        .arg(gresource_xml)
        .output()?;

    Ok(())
}
