#![allow(unused)]
#[derive(Debug)]
pub enum Message<'a> {
    CloseTab(Option<u32>),

    UpdateWindowTitle(Option<&'a str>),
    UpdateTabTitle(Option<u32>, String),

    // Clipboard
    CopyToClipboard(Option<u32>),
    PasteToTerminal(Option<u32>),

    //etc
    UpdateGeometryHints(u32),
}

#[derive(Debug)]
pub enum WindowCommand {
    CloseWindow,
    CloseTab(u32),
    CloseActiveTab,
    UpdateWindowGeometry(u32),

    Clipboard(Clipboard),

    UpdateWindowTitle(Option<String>),
    UpdateTabTitle { page: u32, title: String },
    UpdateWindowColorScheme(String),
}

#[derive(Debug)]
pub enum ApplicationCommand {
    Quit,
    CreateWindow,
}

#[derive(Debug)]
pub enum Clipboard {
    Paste,
    Copy,
}
