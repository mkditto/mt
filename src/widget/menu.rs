use gio::{Menu as GMenu, MenuItem as GMenuItem};
use gtk::{prelude::*, Menu, Popover};

/// Can't subclass the menu super well, so we're doing it the old fashioned way. This strucutre
/// represents the menu available from the dropdown in the top right of the window. This allows
/// you to create new tabs or windows (when that's supported) as well as basic copy/paste or
/// show information about the application.
#[derive(Clone, Debug)]
pub struct MtMenu {
    /// Primary model
    pub model: GMenu,
    /// The sections of the menu
    active_tabs_section: GMenu,
    action_section: GMenu,
    edit_section: GMenu,
    app_section: GMenu,
    quit_section: GMenu,

    tab_profile_model: GMenu,
    win_profile_model: GMenu,
}

impl MtMenu {
    /// Create a new `MtMenu`
    ///
    /// # Arguments
    /// * `default` - The name of the default profile configured in preferences
    /// * `profiles` - The full list of profiles that can be created. If a profile
    ///    with the same name as `default` is included, it will be removed.
    pub fn new(default: &str, profiles: &Vec<String>) -> Self {
        let model = GMenu::new();
        let tab_profile_model = GMenu::new();
        let win_profile_model = GMenu::new();
        let tab_profile_section_model = GMenu::new();
        let win_profile_section_model = GMenu::new();
        // this will remain unused for now
        let active_tabs_section = GMenu::new();
        let action_section = GMenu::new();
        let edit_section = GMenu::new();
        let app_section = GMenu::new();
        let quit_section = GMenu::new();

        let profiles = profiles
            .iter()
            .filter(|&p| p != default)
            .collect::<Vec<&String>>();

        // If more profiles than the default are defined, turn the "New Tab" and "New Window"
        // into submenus that allow you to create a new tab/window with a specific profile.
        // Otherwise, have the "New Tab"/"New Window" options just be items that trigger the
        // default profile.
        if profiles.len() > 0 {
            for profile in profiles {
                tab_profile_section_model.append_item(&GMenuItem::new(
                    Some(profile),
                    Some(&format!("win.new-tab-with-profile(\"{}\")", profile)),
                ));

                win_profile_section_model.append_item(&GMenuItem::new(
                    Some(profile),
                    Some(&format!("win.new-win-with-profile(\"{}\")", profile)),
                ));
            }
            tab_profile_model.append_item(&GMenuItem::new(Some(default), Some("win.new-tab")));
            tab_profile_model.append_section(None, &tab_profile_section_model);

            win_profile_model.append_item(&GMenuItem::new(Some(default), Some("win.new-win")));
            win_profile_model.append_section(None, &win_profile_section_model);
            action_section.append_item(&GMenuItem::new_submenu(
                Some("New Window"),
                &win_profile_model,
            ));
            action_section
                .append_item(&GMenuItem::new_submenu(Some("New Tab"), &tab_profile_model));
        } else {
            action_section.append_item(&GMenuItem::new(Some("New Window"), Some("win.new-win")));
            action_section.append_item(&GMenuItem::new(Some("New Tab"), Some("win.new-tab")));
        }

        action_section.append_item(&GMenuItem::new(
            Some("Close Tab"),
            Some("win.close-active-tab"),
        ));

        edit_section.append_item(&GMenuItem::new(Some("Copy"), Some("win.clipboard-copy")));
        edit_section.append_item(&GMenuItem::new(Some("Paste"), Some("win.clipboard-paste")));
        edit_section.append_item(&GMenuItem::new(Some("Find"), Some("win.find")));

        app_section.append_item(&GMenuItem::new(Some("About"), Some("app.about")));
        app_section.append_item(&GMenuItem::new(
            Some("Preferences"),
            Some("app.preferences"),
        ));

        quit_section.append_item(&GMenuItem::new(Some("Quit"), Some("app.quit")));

        model.append_section(None, &active_tabs_section);
        model.append_section(None, &action_section);
        model.append_section(None, &edit_section);
        model.append_section(None, &app_section);
        model.append_section(None, &quit_section);

        Self {
            model,
            active_tabs_section,
            action_section,
            edit_section,
            app_section,
            quit_section,
            tab_profile_model,
            win_profile_model,
        }
    }

    #[allow(dead_code)]
    pub fn into_popover(&self) -> Popover {
        // Just need to define P as gtk::Widget to get it to compile
        let popover = Popover::from_model::<gtk::Widget, _>(None, &self.model);
        popover.set_halign(gtk::Align::End);
        popover
    }

    pub fn into_menu(&self) -> Menu {
        let menu = Menu::from_model(&self.model);
        menu.set_halign(gtk::Align::End);

        menu
    }
}
