//! This module serves as the home for the 3 window layouts possible with Mt. Some
//! aren't implemented yet (compact and standard), but they are coming
use crate::{
    app::MtApplication,
    config::Layout,
    message::{Clipboard, WindowCommand},
    util,
    widget::{menu::MtMenu, MtTerminal},
};
use glib::{
    clone,
    subclass::{self, prelude::*},
    MainContext, ParamSpec, Sender, Value,
};
use gtk::{
    prelude::*, subclass::prelude::ApplicationWindowImpl as GtkApplicationWindowImpl,
    subclass::prelude::*, AboutDialogBuilder, Application,
    ApplicationWindow as GtkApplicationWindow, License, Window,
};
use libhandy::{
    subclass::prelude::ApplicationWindowImpl as HdyApplicationWindowImpl,
    ApplicationWindow as HdyApplicationWindow, TabView,
};
use once_cell::{sync::Lazy, unsync::OnceCell};
use std::{cell::Cell, ops::Deref, path::PathBuf, rc::Rc};
use vte::traits::TerminalExt;

mod compact;
mod standard;
mod unified;

pub trait WindowContent: std::fmt::Debug {
    fn root(&self) -> &gtk::Box;
    fn tabbar(&self) -> &libhandy::TabBar;
    fn tabview(&self) -> &libhandy::TabView;
    fn set_menu(&self, menu: &MtMenu);
    fn set_title(&self, title: &str);
    fn set_style(&self, old: &str, new: &str);
}

impl std::convert::Into<Rc<dyn WindowContent>> for Layout {
    fn into(self) -> Rc<dyn WindowContent> {
        match self {
            Layout::Unified => Rc::new(unified::UnifiedWindowContent::new()),
            Layout::Compact => Rc::new(compact::CompactWindowContent::new()),
            Layout::Standard => Rc::new(standard::StandardWindowContent::new()),
        }
    }
}

#[derive(Clone)]
pub struct MtApplicationWindowPriv {
    layout: OnceCell<Layout>,
    content: OnceCell<Rc<dyn WindowContent>>,
    sender: OnceCell<Sender<WindowCommand>>,
    active_style_class: Rc<Cell<String>>,
    menu: OnceCell<MtMenu>,
}

#[glib::object_subclass]
impl ObjectSubclass for MtApplicationWindowPriv {
    const NAME: &'static str = "MtApplicationWindow";

    type Type = MtApplicationWindow;
    type ParentType = HdyApplicationWindow;
    type Class = subclass::basic::ClassStruct<Self>;
    type Instance = subclass::basic::InstanceStruct<Self>;

    fn new() -> Self {
        Self {
            layout: OnceCell::new(),
            content: OnceCell::new(),
            sender: OnceCell::new(),
            active_style_class: Rc::new(Cell::new(String::from(""))),
            menu: OnceCell::new(),
        }
    }
}

impl ObjectImpl for MtApplicationWindowPriv {
    fn properties() -> &'static [ParamSpec] {
        static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
            vec![glib::ParamSpec::new_boxed(
                "layout",
                "layout",
                "window layout",
                Layout::static_type(),
                glib::ParamFlags::READWRITE,
            )]
        });

        PROPERTIES.as_ref()
    }

    fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
        log::trace!("Setting property: {}", pspec.name());
        match pspec.name() {
            "layout" => {
                let layout = value.get().expect("type confirmity checked i guess");
                if let Err(e) = self.layout.set(layout) {
                    log::error!("{:?}", e);
                }
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "layout" => self.layout.get().unwrap().to_value(),
            _ => unimplemented!(),
        }
    }

    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        obj.set_title("Mt");
        obj.set_icon_name(Some(crate::app::APPID));
    }
}

impl MtApplicationWindowPriv {
    pub fn tabview(&self) -> Option<&TabView> {
        self.content.get().and_then(|rc| Some(rc.deref().tabview()))
    }

    pub fn active_terminal(&self) -> Option<MtTerminal> {
        self.tabview()
            .and_then(|tv| tv.selected_page())
            .and_then(|sp| sp.child())
            .and_then(|child| child.downcast::<MtTerminal>().ok())
    }

    pub fn create_sender(&self) -> Sender<WindowCommand> {
        self.sender.get().unwrap().clone()
    }
}

impl WidgetImpl for MtApplicationWindowPriv {}
impl ContainerImpl for MtApplicationWindowPriv {}
impl BinImpl for MtApplicationWindowPriv {}
impl WindowImpl for MtApplicationWindowPriv {}
impl GtkApplicationWindowImpl for MtApplicationWindowPriv {}
impl HdyApplicationWindowImpl for MtApplicationWindowPriv {}

glib::wrapper! {
    pub struct MtApplicationWindow(
        ObjectSubclass<MtApplicationWindowPriv>
    ) @extends
        gtk::Widget,
        gtk::Container,
        gtk::Bin,
        gtk::Window,
        GtkApplicationWindow,
        HdyApplicationWindow;
}

impl MtApplicationWindow {
    pub fn new(app: &MtApplication) -> Self {
        let default = app
            .get_default_profile_name()
            .expect("No default profile defined");
        let profiles = app.get_profile_names().expect("No profiles defined");
        let layout = app.get_layout().expect("No layout defined");
        let window: Self = glib::Object::new(&[
            ("application", &app),
            ("name", &"terminal-window"),
            ("layout", &layout),
        ])
        .expect("Failed to create MtApplicationWindow");

        app.add_window(&window);

        window.layout();
        window.setup_gactions();
        window.setup_menu(&default, &profiles);

        log::trace!("{:?}", window);

        window
    }

    fn layout(&self) {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let (sender, receiver) = MainContext::channel::<WindowCommand>(glib::PRIORITY_DEFAULT);

        let layout = priv_.layout.get().expect("No layout defined");
        let content: Rc<dyn WindowContent> = layout.clone().into();
        content.tabview().set_shortcut_widget(Some(self));
        content.tabview().connect_selected_page_notify(
            clone!(@strong sender => move |tabview| {
                if let Some(page) = tabview.selected_page() {
                    let title = page.title();
                    let child = page.child()
                        .map(|child| child.downcast::<MtTerminal>())
                        .and_then(|child| child.ok());

                    log::info!("{:?}", child);

                    if let Err(e) = sender.send(WindowCommand::UpdateWindowTitle(title.map(String::from))) {
                        log::warn!("Failed to update window title {:?}", e);
                    }

                    if let Some(terminal) = child {
                        if let Err(e) = sender.send(WindowCommand::UpdateWindowColorScheme(
                            terminal.profile().colorscheme.clone()
                        ))
                        {
                            log::warn!("Failed to update window colorscheme {:?}", e);
                        }
                    }
                }
            }
        ));

        self.add(content.root());
        priv_.content.set(content).expect("Failed to set content");

        {
            let window = self.clone();
            let window_priv = priv_.clone();
            let active_style_class = priv_.active_style_class.clone();
            receiver.attach(None, move |cmd| {
                log::debug!("Received {:?}", cmd);
                match cmd {
                    WindowCommand::UpdateWindowTitle(title) => window.set_titles(title),
                    WindowCommand::UpdateWindowColorScheme(colorscheme) => {
                        let new_style_class = colorscheme.to_lowercase().replace(" ", "-");
                        let old_style_class = active_style_class.deref().replace(String::from(&new_style_class));

                        if let Some(content) = window_priv.content.get() {
                            if new_style_class != old_style_class {
                                content.deref().set_style(&old_style_class, &new_style_class);
                            } else {
                                log::info!("Given window style class same as current active class, discarding");
                            }
                        } else {
                            log::error!("Window content not defined");
                        }
                    },
                    WindowCommand::Clipboard(action) => {
                        if let Some(terminal) = window_priv.active_terminal() {
                            match action {
                                Clipboard::Copy => terminal.copy(),
                                Clipboard::Paste => terminal.paste(),
                            }
                        } else {
                            log::warn!("No active terminal! Cannot perform clipboard actions!");
                        }
                    }
                    c @ _ => log::info!("Discarding command {:?}", c),
                }
                glib::Continue(true)
            });
        }

        priv_.sender.set(sender).expect("Failed to set sender");
    }

    fn setup_gactions(&self) {
        let window = self.clone().upcast::<gtk::ApplicationWindow>();
        let sender = self.create_sender();

        util::action(
            &window,
            "new-tab",
            clone!(@weak self as window => move |_, _| {
                window.terminal(None, None, None);
            }),
            None,
        );

        util::action(
            &window,
            "new-tab-with-profile",
            clone!(@weak self as window => move |_, name| {
                let profile = name.and_then(|variant| variant.str());
                window.terminal(profile, None, None);
            }),
            Some(&glib::VariantType::new("s").unwrap()),
        );

        util::action(
            &window,
            "close-active-tab",
            clone!(@weak self as window => move |_, _| {
                window.close_active_tab();
            }),
            None,
        );

        util::action(
            &window,
            "clipboard-copy",
            clone!(@strong sender => move |_, _| {
                if let Err(e) = sender.send(WindowCommand::Clipboard(Clipboard::Copy)) {
                    log::error!("Failed to copy {:?}", e);
                }
            }),
            None,
        );

        util::action(
            &window,
            "clipboard-paste",
            clone!(@strong sender => move |_, _| {
                if let Err(e) = sender.send(WindowCommand::Clipboard(Clipboard::Paste)) {
                    log::error!("Failed to paste {:?}", e);
                }
            }),
            None,
        );
    }

    pub fn terminal(&self, profile: Option<&str>, cwd: Option<PathBuf>, cmd: Option<Vec<PathBuf>>) {
        let app = self.application().unwrap();
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let tabview = priv_.tabview().unwrap();

        let terminal = app
            .clone()
            .downcast::<MtApplication>()
            .ok()
            .and_then(|mtapp| {
                let tparams = profile
                    .and_then(|name| mtapp.get_profile(name))
                    .or(mtapp.get_default_profile())?;
                log::debug!("Created TParams {:?}", tparams);
                Some(MtTerminal::new(tparams, cwd, cmd, self.create_sender()))
            });

        if let Some(terminal) = terminal {
            if let Some(page) = tabview.append(&terminal) {
                let sender = self.create_sender();
                terminal.show();
                terminal.connect_window_title_changed(
                    clone!(@weak page, @weak tabview => move |vte| {
                        let title = vte.window_title();
                        let title = title.as_deref();
                        page.set_title(title);

                        if let Some(selected_page) = tabview.selected_page() {
                            if tabview.page_position(&page) == tabview.page_position(&selected_page) {
                                if let Err(e) = sender.send(WindowCommand::UpdateWindowTitle(title.map(String::from))) {
                                    log::error!("Failed to update window title {:?}", e);
                                }
                            }
                        }
                    })
                );
            } else {
                log::error!("Unable to append MtTerminal to HdyTabView");
            }
        } else {
            log::error!("Unable to create MtTerminal");
        }

        log::trace!("Number of pages: {}", tabview.n_pages());
        log::trace!("Currently selected page: {:?}", tabview.selected_page())
    }

    /// In order to do this, we need access to the list of profiles and the default profile
    /// name. I didn't really think this through so we can't actually access the application
    /// structure which stores the config from within the `constructed()` function. So I have
    /// to delay the menu construction until we have access to the application, which means
    /// we're going to have to call this function from within the `MtApplicationWindow::new()`
    /// function or later. This a really ugly solution, I hate it, I need to better reorganize
    /// this. Considering I need to soon support multiple layouts and eventually port to GKT4,
    /// this window class will get re-written anyway. I need to start planning what that is
    /// going to look like. Perhaps I need to fully separate out the model (like I should have
    /// done in the beginning) and pass a pointer to it into the window. Or maybe have separate
    /// `ApplicationContext` and `WindowContext` structures which represent internal states.
    fn setup_menu(&self, default: &str, profiles: &Vec<String>) {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let menu = MtMenu::new(default, profiles);

        // let menu_button = priv_.content.get().and_then(|c| Some(c.menubutton()));

        if let Some(content) = priv_.content.get() {
            content.set_menu(&menu);
            priv_.menu.set(menu).expect("Failed to set menu");
        }
        // if let Some(menu_button) = menu_button {
        //     menu_button.set_popup(Some(&menu.into_menu()));
        //     priv_.menu.set(menu).expect("Failed to set menu");
        // }
    }

    pub fn set_titles(&self, title: Option<String>) {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let title = title.unwrap_or_default();

        self.set_title(&title);
        priv_
            .content
            .get()
            .and_then(|rc| Some(rc.deref().set_title(&title)));
    }

    pub fn create_sender(&self) -> Sender<WindowCommand> {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        priv_.create_sender()
    }

    pub fn close_active_tab(&self) {
        let app = self.application().unwrap();
        let priv_ = MtApplicationWindowPriv::from_instance(self);

        if let Some(tabview) = priv_.tabview() {
            if tabview.n_pages() == 1 {
                log::debug!("Only 1 page open: Quitting App");
                app.quit()
            } else if let Some(active_page) = tabview.selected_page() {
                tabview.close_page(&active_page);
            }
        }
    }

    pub fn active_terminal(&self) -> Option<MtTerminal> {
        let priv_ = MtApplicationWindowPriv::from_instance(self);

        priv_.active_terminal()
    }

    pub fn focus_active_tab(&self) {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let terminal = priv_
            .tabview()
            .and_then(|view| view.selected_page())
            .and_then(|page| page.child())
            .and_then(|child| child.downcast::<MtTerminal>().ok());

        if let Some(terminal) = terminal {
            terminal.focus();
        }
    }
}

pub fn about_dialog<T, G>(parent: &T, app: &G)
where
    T: IsA<Window>,
    G: IsA<Application>,
{
    let dialog = AboutDialogBuilder::new()
        .transient_for(parent)
        .application(app)
        .deletable(true)
        .decorated(true)
        .program_name(crate::app::APPID)
        .artists(vec![String::from("Michael Ditto")])
        .authors(vec![String::from("Michael Ditto")])
        .version(env!("CARGO_PKG_VERSION"))
        .license_type(License::Gpl20Only)
        .logo_icon_name(&crate::app::APPID)
        .modal(true)
        .build();

    match dialog.run() {
        _ => dialog.emit_close(),
    }
}
